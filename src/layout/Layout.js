import BreatCome from "components/BreatCome";
import Header from "components/Header";
import TopHeader from "components/TopHeader";
import Footer from "pages/Home/components/Footer";
import React from "react";

export default function Layout({ pageTitle, children }) {
  return (
    <div>
      <TopHeader />
      <Header />
      <BreatCome pageTitle={pageTitle} />
      {children}
      <Footer />
    </div>
  );
}
