import logo from "../assets/images/logo/logo-white.png";
import logoBlack from "../assets/images/logo/logo.png";
import shape1 from "../assets/images/shape1.png";
import sdt from "../assets/images/sdt.png";
import video1 from "../assets/images/video1.jpg";
import vp1 from "../assets/images/vp1.jpg";
import vp2 from "../assets/images/vp2.jpg";
import vp3 from "../assets/images/vp3.jpg";
import vp4 from "../assets/images/vp4.jpg";
import tab1 from "../assets/images/portfolio/tab1.jpg";
import tab2 from "../assets/images/portfolio/tab2.jpg";
import tab3 from "../assets/images/portfolio/tab3.jpg";
import slider4 from "../assets/images/slider/slider-4.jpg";
import safeness from "../assets/images/partner/safeness.png";
import sigma from "../assets/images/partner/sigma.png";
import preventis from "../assets/images/partner/preventis.png";
export default {
  logo,
  shape1,
  sdt,
  video1,
  vp1,
  vp2,
  vp3,
  vp4,
  tab1,
  tab2,
  tab3,
  slider4,
  logoBlack,
  partner: {
    safeness,
    sigma,
    preventis,
  },
};
