export default {
  email: "contact@agility.com.tn",
  tel: "+216 22 835 520",
  address: "Route de Gabes KM 9 THINA MF : 1704464 B-A-M-000",
  appName: "Agility",
  menu: {
    home: "Accueil",
    company: "Entreprise",
    services: "Services",
    partner: "Nos partenaires",
    contact: "Contacter nous",
  },
  home: {
    title: "Nous fournissons les meilleures",
    title2: "Solutions informatiques de tous types",
    subTitle:
      "Nous proposons des solutions qui peuvent vous aider à déployer et à développer votre infrastructure informatique du début à la fin, en vous apportant les meilleures innovations technologiques.",
    aboutUs: "À PROPOS DE NOUS",
    aboutUsTitle: `Préparez votre réussite`,
    aboutUsTitle2: `Fournir les meilleures .`,
    aboutUsDescription:
      "Les systèmes vocaux et de données sont essentiels au succès ou à l'échec de la plupart des entreprises. toutes les entreprises fournissent des ordinateurs portables, des téléphones portables.",
    aboutUsSubDescription:
      "Nous proposons les meilleures solutions d'infrastructure IT et Data Center à haute valeur technologique, grâce à notre expertise en sécurité informatique, communication unifiée et stockage de données, selon les standards internationaux.",
    services: "Services",
    ourServices: "Nos services",
    digitalMarketing: "Le marketing numérique",
    digitalMarketingDescription: `Le marketing numérique fait référence à tout le marketing
    techniques utilisées sur les médias et canaux numériques pour
    promouvoir les produits et services. En numérique
    marketing, une notion essentielle émerge,
    l'interactivité. On peut aussi parler de web
    marketing puisque la différence est principalement liée
    à l'environnement, ici Internet`,
    webHosting: "Hibergement web",
    cyberSecurity: "La cyber-sécurité",
    smartCamera: "Surveillance intelligente",
    webHostingTitle: "Fournisseur d'hébergement Web",
    webHostingDescription: `Vous êtes-vous déjà demandé: «Qu'est-ce que l'hébergement Web?» Si
    vous possédez un domaine et souhaitez le partager avec le
    monde, vous avez besoin de services d'hébergement Web. Tous
    site Web a besoin d'un «domicile» en ligne, un endroit où
    les données peuvent être stockées et un moyen de mettre ces données
    en ligne via un fournisseur d'hébergement Web. Avec
    des options allant de gratuit (ou presque) à
    des centaines par mois, les hébergeurs fournissent le
    essentiels pour obtenir des sites Web de toutes sortes
    vivre sur Internet. Construire un site Web, c'est
    à portée de presque tout le monde, il y a donc
    options d'hébergement pour les utilisateurs à tous les niveaux de
    compétences et ambitions, des blogueurs occasionnels aux
    développeurs dédiés travaillant à construire des complexes
    sites à partir de zéro.`,
    cyberSecurityTitle: "Sécurité du serveur de données",
    cyberSecurityDescription: `Grâce à une longue expérience dans le domaine de
    sécurité informatique et à de nombreux prestigieux
    certifications, 3S garantit l'intégrité,
    confidentialité et disponibilité de votre
    Système d'Information. Nous certifions que les données que vous
    gérer et échanger en interne, avec votre
    partenaires et clients, est terminée. Nous
    garantir la confidentialité, donner accès à
    les ressources de l'entreprise partout, à tout moment et à travers
    tout appareil, uniquement aux personnes autorisées. Nous
    assurer la disponibilité, une garantie de bonne
    fonctionnement et continuité des affaires de votre
    Système d'Information.`,
    smartCameraTitle: "Solution de caméra de surveillance intelligente",
    smartCamerDescription: `Une solution de vidéosurveillance classique est limitée
    pour recevoir des images en direct et visionner des enregistrements
    vidéos. Au-delà de 10 caméras, un agent de sécurité
    ne peut pas se concentrer pendant plus de 15 minutes. Dans
    en cas d'incident, l'agent doit se rendre
    à travers toutes les vidéos enregistrées. Analytique
    la vidéo se distingue par son intelligence,
    réactivité et proactivité. Il peut analyser
    images des caméras CCTV, détecter les
    comportement en temps réel et assurer une
    surveillance de la sécurité. L'analytique peut extraire
    informations utiles sur les objets, les personnes,
    véhicules et comportements présents dans un complexe
    scène vidéo. Ces informations seront utilisées,
    selon des règles prédéfinies pour appliquer
    les actions appropriées`,
  },
  company: {
    yearExperice: "30 ANS D'EXPÉRIENCE",
    companySubDescription: `Le groupe Agility offre tous les services en matière d’informatique, gestion de données et hardware. Des solutions informatiques à la pointe de la technologie, c’est notre métier !
    `,
    services: {
      responsiveDesign: "Conception réactive",
      responsiveDesginDescriptio: ``,
      onlineSupport: "Assistance en ligne 24h / 24 et 7j / 7",
      onLigneSupportDescription: ``,
      qualityProduct: "Produit de qualité",
      qualityProductDescription: "",
      productivSoftware: "Logiciel Productiv",
      productivSoftwareDescription: "",
    },
  },
};
