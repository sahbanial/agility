import React from "react";

export default function BreatCome({ pageTitle }) {
  return (
    <div className="breatcome_area d-flex align-items-center">
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <div className="breatcome_title">
              <div className="breatcome_title_inner pb-2">
                <h2>{pageTitle}</h2>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
