import { useAppContext } from "context/AppContext";
import React from "react";
import { UilPhone } from "@iconscout/react-unicons";
import { UilMapMarker } from "@iconscout/react-unicons";
import { UilEnvelope } from "@iconscout/react-unicons";
export default function TopHeader() {
  const { data } = useAppContext();
  return (
    <div className="header_top_menu p-2 bg_color">
      <div className="container">
        <div className="row">
          <div className="col-lg-12 col-sm-12">
            <div className="header_top_menu_address">
              <div className="header_top_menu_address_inner">
                <ul>
                  <li>
                    <span>
                      <UilEnvelope color="white" size={16} />
                      <span className="text-white ml-1 font-small-1">
                        {data?.email}
                      </span>
                    </span>
                  </li>
                  <li>
                    <span className="ml-1">
                      <UilMapMarker color="white" size={16} />
                      <span className="text-white ml-1  font-small-1">
                        {data?.address}
                      </span>
                    </span>
                  </li>
                  <li>
                    <span className="ml-1">
                      <UilPhone color="white" size={16} />
                      <span className="text-white ml-1  font-small-1">
                        {data?.tel}
                      </span>
                    </span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
