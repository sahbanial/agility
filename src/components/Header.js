import Images from "config/Images";
import React from "react";
import data from "config/data";

export default function Header() {
  return (
    <div>
      <div
        id="sticky-header"
        class="techno_nav_manu d-md-none d-lg-block d-sm-none d-none"
      >
        <div className="container">
          <div className="row align-items-center">
            <div className="col-md-3">
              <div className="logo">
                <a className="logo_img" href="/" title="techno">
                  <img src={Images.logoBlack} alt className="logo" />
                </a>
                <a className="main_sticky" href="/" title="techno">
                  <img src={Images.logoBlack} alt="astute" />
                </a>
              </div>
            </div>
            <div className="col-md-9">
              <nav className="techno_menu">
                <ul className="nav_scroll">
                  <li>
                    <a href="/home">{data?.menu.home}</a>
                  </li>
                  <li>
                    <a href="/company">{data?.menu.company}</a>
                  </li>
                  <li>
                    <a href="/service">{data?.menu.services}</a>
                  </li>
                  <li>
                    <a href="/partners">{data?.menu.partner}</a>
                  </li>

                  <li>
                    <a href="/contact">{data?.menu.contact}</a>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
