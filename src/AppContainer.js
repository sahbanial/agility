import React from "react"
import logo from "./assets/images/logo.png";
import video1 from "./assets/images/video1.jpg";
function App() {
  return (
  <React.Fragment>
  <div id="sticky-header" className="techno_nav_manu transparent_menu white d-md-none d-lg-block d-sm-none d-none">
    <div className="container">
      <div className="row">
        <div className="col-md-3">
          <div className="logo mt-4">
            <a className="logo_img" href="index.html" title="techno">
              <img src={logo} alt />
            </a>
            <a className="main_sticky" href="index.html" title="techno">
              <img src={logo} alt="astute" />
            </a>
          </div>
        </div>
        <div className="col-md-9">
          <nav className="techno_menu">
            <ul className="nav_scroll">
              <li><a href="#home">Home</a>
                <ul className="sub-menu">
                  <li><a href="index.html">Home  One</a></li>
                  <li><a href="#new-demo.html">Added New Demo</a>
                    <ul className="sub-menu">
                      <li><a href="index-11.html">Home  Eleven New</a></li>	
                      <li><a href="index-12.html">Home Twelve New</a></li>
                      <li><a href="index-13.html">Home Thirteen New</a></li>
                      <li><a href="data-science.html">Data Science New</a></li>
                      <li><a href="machine-learning.html">Machine Learning New</a></li>
                      <li><a href="affiliate-intelligent.html">Affiliate Intelligent New</a></li>
                      <li><a href="home-dark.html">Dark Version New</a></li>
                      <li><a href="index-14.html">Home Animation New</a></li>
                      <li><a href="landing-1.html">Landing One New</a></li>
                      <li><a href="landing-2.html">Landing Two New</a></li>
                      <li><a href="landing-3.html">Landing Three New</a></li>
                    </ul>
                  </li>
                  <li><a href="index-2.html">Home  Two</a></li>
                  <li><a href="index-3.html">Home  Three</a></li>
                  <li><a href="index-4.html">Home  Four</a></li>
                  <li><a href="index-5.html">Home  Five</a></li>
                  <li><a href="index-6.html">Home  Six</a></li>
                  <li><a href="index-7.html">Home  Seven</a></li>
                  <li><a href="index-8.html">Home  Eight</a></li>
                  <li><a href="index-9.html">Home  Nine</a></li>
                  <li><a href="index-10.html">Home  Ten</a></li>
                </ul>
              </li>
              <li><a href="#company.html">Company</a>
                <ul className="sub-menu">
                  <li><a href="about.html">About One</a></li>
                  <li><a href="about-2.html">About Two</a></li>
                  <li><a href="about-3.html">About Three</a></li>
                  <li><a href="team.html">Our Team</a></li>
                  <li><a href="team-details.html">Team Details</a></li>
                  <li><a href="why-choose-us.html">Whay Choose Us</a></li>
                  <li><a href="case-study.html">Case Study</a></li>
                  <li><a href="case-study-details.html">Case Study Details</a></li>
                  <li><a href="portfolio.html">Portfolio</a></li>
                  <li><a href="pricing.html">Pricing</a></li>
                  <li><a href="faq.html">FAQ</a></li>
                  <li><a href="404.html">Error</a></li>
                </ul>
              </li>
              <li><a href="#service.html">Services</a>
                <ul className="sub-menu">
                  <li><a href="service-1.html">Service One</a></li>
                  <li><a href="service-2.html">Service Two</a></li>
                  <li><a href="service-3.html">Service Three</a></li>
                  <li><a href="service-4.html">Service Four</a></li>
                  <li><a href="service-details.html">Service Details</a></li>
                </ul>
              </li>
              <li><a href="#solution.html">IT Solution</a>
                <ul className="sub-menu">
                  <li><a href="managed-service.html">Managed IT Service</a></li>
                  <li><a href="it-service.html">IT Service</a></li>
                  <li><a href="industries.html">Industries</a></li>
                  <li><a href="business-solution.html">Business Solution</a></li>
                  <li><a href="product-design.html">Product Design</a></li>
                  <li><a href="service-details.html">IT Service Details</a></li>
                </ul>
              </li>
              <li><a href="#elements.html">Element</a>
                <ul className="sub-menu">
                  <li><a href="service-element.html">Service Box</a></li>
                  <li><a href="info-box-element.html">Info Box</a></li>
                  <li><a href="team-element.html">Teams</a></li>
                  <li><a href="case-study-element.html">Case Study</a></li>
                  <li><a href="process-element.html">Process</a></li>
                  <li><a href="testimonial-element.html">Testimonials</a></li>
                  <li><a href="pricing-element.html">Pricing</a></li>
                  <li><a href="counter-element.html">Counters</a></li>
                  <li><a href="call-do-action-element.html">Call Do Action</a></li>
                  <li><a href="brand-element.html">Brands</a></li>
                  <li><a href="blog-element.html">Blogs</a></li>
                </ul>
              </li>
              <li><a href="#blog">Blog </a>
                <ul className="sub-menu">
                  <li><a href="blog-gird.html">Blog Gird</a></li>
                  <li><a href="blog-list.html">Blog List</a></li>
                  <li><a href="blog-left-sidebar.html">Blog Left Sidebar</a></li>
                  <li><a href="blog-right-sidebar.html">Blog Right Sidebar</a></li>
                  <li><a href="blog-details.html">Blog Details</a></li>
                </ul>
              </li>
              <li><a href="#contact">Contact</a>
                <ul className="sub-menu">
                  <li><a href="contact.html">Contact One</a></li>
                  <li><a href="contact-2.html">Contact Two New</a></li>
                  <li><a href="contact-3.html">Contact Three New</a></li>
                  <li><a href="contact-4.html">Contact Four New</a></li>
                  <li><a href="contact-5.html">Contact Five New</a></li>
                  <li><a href="contact-6.html">Contact Six New</a></li>
                </ul>
              </li>
            </ul>
            <div className="donate-btn-header">
              <a className="dtbtn" href="#">Get A Quote</a>	
            </div>				
          </nav>
        </div>
      </div>
    </div>
  </div>

  <div className="mobile-menu-area d-sm-block d-md-block d-lg-none ">
    <div className="mobile-menu">
      <nav className="techno_menu">
        <ul className="nav_scroll">
          <li><a href="#home">Home</a>
            <ul className="sub-menu">
              <li><a href="index.html">Home  One</a></li>
              <li><a href="#new-demo.html">Added New Demo</a>
                <ul className="sub-menu">
                  <li><a href="index-11.html">Home  Eleven New</a></li>	
                  <li><a href="index-12.html">Home Twelve New</a></li>
                  <li><a href="index-13.html">Home Thirteen New</a></li>
                  <li><a href="data-science.html">Data Science New</a></li>
                  <li><a href="machine-learning.html">Machine Learning New</a></li>
                  <li><a href="affiliate-intelligent.html">Affiliate Intelligent New</a></li>
                  <li><a href="home-dark.html">Dark Version New</a></li>
                  <li><a href="index-14.html">Home Animation New</a></li>
                  <li><a href="landing-1.html">Landing One New</a></li>
                  <li><a href="landing-2.html">Landing Two New</a></li>
                  <li><a href="landing-3.html">Landing Three New</a></li>
                </ul>
              </li>
              <li><a href="index-2.html">Home  Two</a></li>
              <li><a href="index-3.html">Home  Three</a></li>
              <li><a href="index-4.html">Home  Four</a></li>
              <li><a href="index-5.html">Home  Five</a></li>
              <li><a href="index-6.html">Home  Six</a></li>
              <li><a href="index-7.html">Home  Seven</a></li>
              <li><a href="index-8.html">Home  Eight</a></li>
              <li><a href="index-9.html">Home  Nine</a></li>
              <li><a href="index-10.html">Home  Ten</a></li>
            </ul>
          </li>
          <li><a href="#company.html">Company</a>
            <ul className="sub-menu">
              <li><a href="about.html">About One</a></li>
              <li><a href="about-2.html">About Two</a></li>
              <li><a href="about-3.html">About Three</a></li>
              <li><a href="team.html">Our Team</a></li>
              <li><a href="team-details.html">Team Details</a></li>
              <li><a href="why-choose-us.html">Whay Choose Us</a></li>
              <li><a href="case-study.html">Case Study</a></li>
              <li><a href="case-study-details.html">Case Study Details</a></li>
              <li><a href="portfolio.html">Portfolio</a></li>
              <li><a href="pricing.html">Pricing</a></li>
              <li><a href="faq.html">FAQ</a></li>
              <li><a href="404.html">Error</a></li>
            </ul>
          </li>
          <li><a href="#service.html">Services</a>
            <ul className="sub-menu">
              <li><a href="service-1.html">Service One</a></li>
              <li><a href="service-2.html">Service Two</a></li>
              <li><a href="service-3.html">Service Three</a></li>
              <li><a href="service-4.html">Service Four</a></li>
              <li><a href="service-details.html">Service Details</a></li>
            </ul>
          </li>
          <li><a href="#solution.html">IT Solution</a>
            <ul className="sub-menu">
              <li><a href="managed-service.html">Managed IT Service</a></li>
              <li><a href="it-service.html">IT Service</a></li>
              <li><a href="industries.html">Industries</a></li>
              <li><a href="business-solution.html">Business Solution</a></li>
              <li><a href="product-design.html">Product Design</a></li>
              <li><a href="service-details.html">IT Service Details</a></li>
            </ul>
          </li>
          <li><a href="#elements.html">Element</a>
            <ul className="sub-menu">
              <li><a href="service-element.html">Service Box</a></li>
              <li><a href="info-box-element.html">Info Box</a></li>
              <li><a href="team-element.html">Teams</a></li>
              <li><a href="case-study-element.html">Case Study</a></li>
              <li><a href="process-element.html">Process</a></li>
              <li><a href="testimonial-element.html">Testimonials</a></li>
              <li><a href="pricing-element.html">Pricing</a></li>
              <li><a href="counter-element.html">Counters</a></li>
              <li><a href="call-do-action-element.html">Call Do Action</a></li>
              <li><a href="brand-element.html">Brands</a></li>
              <li><a href="blog-element.html">Blogs</a></li>
            </ul>
          </li>
          <li><a href="#blog">Blog </a>
            <ul className="sub-menu">
              <li><a href="blog-gird.html">Blog Gird</a></li>
              <li><a href="blog-list.html">Blog List</a></li>
              <li><a href="blog-left-sidebar.html">Blog Left Sidebar</a></li>
              <li><a href="blog-right-sidebar.html">Blog Right Sidebar</a></li>
              <li><a href="blog-details.html">Blog Details</a></li>
            </ul>
          </li>
          <li><a href="#contact">Contact</a>
            <ul className="sub-menu">
              <li><a href="contact.html">Contact One</a></li>
              <li><a href="contact-2.html">Contact Two New</a></li>
              <li><a href="contact-3.html">Contact Three New</a></li>
              <li><a href="contact-4.html">Contact Four New</a></li>
              <li><a href="contact-5.html">Contact Five New</a></li>
              <li><a href="contact-6.html">Contact Six New</a></li>
            </ul>
          </li>
        </ul>
      </nav>
    </div>
  </div>

  <div className="slider_area d-flex align-items-center slider3" id="home">
    <div className="container">
      <div className="row">
        <div className="col-lg-12">
          <div className="single_slider">
            <div className="slider_content">
              <div className="slider_text">
                <div className="slider_text_inner">
                  <h1> We Provide The Best </h1>
                  <h1>All Kind IT Solution</h1>
                </div>
                <div className="slider_text_desc pt-4">
                  <p>Morem upsum dolor set amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut laboret dolore magna aliqua.</p>
                </div>
                <div className="slider_button pt-5 d-flex">
                  <div className="button">
                    <a href="#">How IT Work <i className="fa fa-long-arrow-right" /></a>
                  </div>
                </div>
                <div className="slider-video two">
                  <div className="video-icon">
                    <a className="video-vemo-icon venobox vbox-item" data-vbtype="youtube" data-autoplay="true" href="https://youtu.be/BS4TUd7FJSg"><i className="fa fa-play" /></a>
                  </div>
                </div>
              </div>
            </div>
            <div className="single_slider_shape">
              <div className="single_slider_shape_image">
                <img src={import("./assets/images/shape1.png")} alt />
              </div>
            </div>
            <div className="single_slider_rot">
              <div className="single_slider_rot_inner rotateme">
                <img src={require("./assets/images/sdt.png")} alt />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div className="about_area pt-70 pb-70">
    <div className="container">
      <div className="row">
        <div className="col-lg-6 col-md-6 col-sm-12 col-xs-6">
          <div className="section_title text_left mb-40 mt-3">
            <div className="section_sub_title uppercase mb-3">
              <h6>ABOUT US</h6>
            </div>
            <div className="section_main_title">
              <h1>Preparing For Your Success</h1>
              <h1>Provide Best <span>IT Solutions.</span></h1>
            </div>
            <div className="em_bar">
              <div className="em_bar_bg" />
            </div>
            <div className="section_content_text bold pt-5">
              <p>Voice and Data Systems are crucial to the success or failure of most businesses. any companies provide laptops, cell phones.</p>
            </div>
          </div>
          <div className="singel_about_left mb-30">
            <div className="singel_about_left_inner mb-3">
              <div className="singel-about-content boder pl-4">
                <p>The standard chunk of Lorem Ipsum used since the 1500s is and reproduced below for those interested. Sections 1.10.32 and also 1.10.33 from “de Finibus Bonorum et Malorum" by Cicero are alse reproduced in their exact original form, accompanied</p>
                <p>The standard chunk of Lorem Ipsum used since the 1500s is and reproduced below for those interested</p>
              </div>
            </div>
            <div className="singel_about_left_inner pl-4">
              <div className="button two">
                <a href="#">More Details</a>
              </div>
            </div>
          </div>	
        </div>
        <div className="col-lg-6 col-md-6 col-sm-12 col-xs-6">
          <div className="single_video">
            <div className="video_thumb text_center mt-160 ml-100">
              <img src={video1} alt />
              <div className="main_video">
                <div className="video-icon">
                  <a className="video-vemo-icon venobox vbox-item" data-vbtype="youtube" data-autoplay="true" href="https://youtu.be/BS4TUd7FJSg"><i className="fa fa-play" /></a>
                </div>
              </div>
            </div>
            <div className="video_shape">
              <div className="video_shape_thumb1 bounce-animate">
                <img src={require("./assets/images/vp1.jpg")} alt style={{height:"500px",width:"500px"}} />
              </div>
            </div>
            <div className="video_shape">
              <div className="video_shape_thumb2 bounce-animate">
                <img src={require("./assets/images/vp2.jpg")} alt />
              </div>
            </div>
            <div className="video_shape">
              <div className="video_shape_thumb3 bounce-animate">
                <img src={require("./assets/images/vp3.jpg")} alt />
              </div>
            </div>
            <div className="video_shape">
              <div className="video_shape_thumb4 bounce-animate">
                <img src={require("./assets/images/vp4.jpg")}  />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>	
  </div>

  <div className="service_area bg_color2 pt-85 pb-70">
    <div className="container">
      <div className="row">
        <div className="col-lg-4 col-md-6 col-sm-12">
          <div className="service_style_two mb-4">
            <div className="service_style_two_number mr-60">
              <h5>01</h5>
            </div>
            <div className="service_style_two_content">
              <div className="service_style_two_title pb-3">
                <h4>Marketing Strategy &amp; Analysis</h4>
              </div>
              <div className="service_style_two_text">
                <p>Whether bringing new amazing products and services to market discovering new ways to make mature products.</p>
              </div>
              <div className="service_style_two_button">
                <a href="#">Read More <i className="fa fa-long-arrow-right" /></a>
              </div>
            </div>
          </div>
        </div>
        <div className="col-lg-4 col-md-6 col-sm-12">
          <div className="service_style_two mb-4">
            <div className="service_style_two_number mr-60">
              <h5>02</h5>
            </div>
            <div className="service_style_two_content">
              <div className="service_style_two_title pb-3">
                <h4>Web Design &amp; Development</h4>
              </div>
              <div className="service_style_two_text">
                <p>Whether bringing new amazing products and services to market discovering new ways to make mature products.</p>
              </div>
              <div className="service_style_two_button">
                <a href="#">Read More <i className="fa fa-long-arrow-right" /></a>
              </div>
            </div>
          </div>
        </div>
        <div className="col-lg-4 col-md-6 col-sm-12">
          <div className="service_style_two mb-4">
            <div className="service_style_two_number mr-60">
              <h5>03</h5>
            </div>
            <div className="service_style_two_content">
              <div className="service_style_two_title pb-3">
                <h4>SEO &amp; Digital Marketing</h4>
              </div>
              <div className="service_style_two_text">
                <p>Whether bringing new amazing products and services to market discovering new ways to make mature products.</p>
              </div>
              <div className="service_style_two_button">
                <a href="#">Read More <i className="fa fa-long-arrow-right" /></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
 
  <div className="tab_top_area pt-100 pb-150" style={{backgroundImage: 'url(assets/images/slider/slider-4.jpg)'}}>
    <div className="container">
      <div className="row ">
        <div className="col-lg-12">
          <div className="section_title text_center white mb-55">
            <div className="section_sub_title uppercase mb-3">
              <h6>SERVICES</h6>
            </div>
            <div className="section_main_title">
              <h1>Provide Exclusive Services</h1>
            </div>
            <div className="em_bar">
              <div className="em_bar_bg" />
            </div>
            <div className="section_content_text pt-4">
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed eiusm tempor incididunt ut labore et dolore magna aliqua. Ut enim advis minim veniam, quis nostrud exercitat</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div className="tab_area pb-100">
    <div className="container">
      <div className="row nagative_margin2">
        <div className="col-lg-12">
          <div className="tab_content">
            <ul className="nav nav-tabs" role="tablist">
              <li className="nav-item">
                <a className="nav-link active" data-toggle="tab" href="#tabs-1" role="tab"> <i className="fa fa-laptop" /> Digital Marketing</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" data-toggle="tab" href="#tabs-2" role="tab"><i className="fa fa-laptop" /> Web Hosting</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" data-toggle="tab" href="#tabs-3" role="tab"><i className="fa fa-database" /> Data Security</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" data-toggle="tab" href="#tabs-4" role="tab"><i className="fa fa-html5" /> Web Development</a>
              </li>
            </ul>{/* Tab panes */}
            <div className="tab-content pt-3">
              <div className="tab-pane active mt-60" id="tabs-1" role="tabpanel">
                <div className="row">
                  <div className="col-lg-6">
                    <div className="tab_thumb">
                      <img src={require("./assets/images/portfolio/tab2.jpg")} alt />
                    </div>
                  </div>
                  <div className="col-lg-6">
                    <div className="tab_content ml-3">
                      <div className="tab_content_title pb-4">
                        <h4>Digital Marketing</h4>
                      </div>
                      <div className="tab_content_text">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed eiusm tempor incididunt ut labore et dolore magna aliqua. Ut enim advis minim veniam, quis nostrud exercitat ullamco But I must explain to you how all this mistaken idea of denouncing</p>
                        <p>Porem tpsum dolor sit amet, consectetur adipisicing elit, sed eiusm morem wpsum solor sit amet, consectetur adipisicing elit, sed eiusm tempor incididunt ut labore et dolore magna</p>
                      </div>
                      <div className="tab_content_sub_text">
                        <ul>
                          <li> <i className="fa fa-long-arrow-right" /> Appropriately empower dynamic leadershp</li>
                          <li> <i className="fa fa-long-arrow-right" /> Globally myocardinate interactive supply chains</li>
                          <li> <i className="fa fa-long-arrow-right" /> Globally revolutionize global sources through</li>
                        </ul>
                      </div>
                      <div className="tab_content_button mt-40">
                        <div className="button">
                          <a href="#">Get A Quote<i className="fa fa-long-arrow-right" /></a>
                          <a className="active" href="#">Details View <i className="fa fa-long-arrow-right" /></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="tab-pane mt-60" id="tabs-2" role="tabpanel">
                <div className="row">
                  <div className="col-lg-6">
                    <div className="tab_thumb">
                      <img src="assets/images/portfolio/tab1.jpg" alt />
                    </div>
                  </div>
                  <div className="col-lg-6">
                    <div className="tab_content ml-3">
                      <div className="tab_content_title pb-4">
                        <h4>Web Hosting Provide</h4>
                      </div>
                      <div className="tab_content_text">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed eiusm tempor incididunt ut labore et dolore magna aliqua. Ut enim advis minim veniam, quis nostrud exercitat ullamco But I must explain to you how all this mistaken idea of denouncing</p>
                        <p>Porem tpsum dolor sit amet, consectetur adipisicing elit, sed eiusm morem wpsum solor sit amet, consectetur adipisicing elit, sed eiusm tempor incididunt ut labore et dolore magna</p>
                      </div>
                      <div className="tab_content_sub_text">
                        <ul>
                          <li> <i className="fa fa-long-arrow-right" /> Appropriately empower dynamic leadershp</li>
                          <li> <i className="fa fa-long-arrow-right" /> Globally myocardinate interactive supply chains</li>
                          <li> <i className="fa fa-long-arrow-right" /> Globally revolutionize global sources through</li>
                        </ul>
                      </div>
                      <div className="tab_content_button mt-40">
                        <div className="button">
                          <a href="#">Get A Quote<i className="fa fa-long-arrow-right" /></a>
                          <a className="active" href="#">Details View <i className="fa fa-long-arrow-right" /></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="tab-pane mt-60" id="tabs-3" role="tabpanel">
                <div className="row">
                  <div className="col-lg-6">
                    <div className="tab_content">
                      <div className="tab_content_title pb-4">
                        <h4>Data Server Security</h4>
                      </div>
                      <div className="tab_content_text">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed eiusm tempor incididunt ut labore et dolore magna aliqua. Ut enim advis minim veniam, quis nostrud exercitat ullamco But I must explain to you how all this mistaken idea of denouncing</p>
                        <p>Porem tpsum dolor sit amet, consectetur adipisicing elit, sed eiusm morem wpsum solor sit amet, consectetur adipisicing elit, sed eiusm tempor incididunt ut labore et dolore magna</p>
                      </div>
                      <div className="tab_content_sub_text">
                        <ul>
                          <li> <i className="fa fa-long-arrow-right" /> Appropriately empower dynamic leadershp</li>
                          <li> <i className="fa fa-long-arrow-right" /> Globally myocardinate interactive supply chains</li>
                          <li> <i className="fa fa-long-arrow-right" /> Globally revolutionize global sources through</li>
                        </ul>
                      </div>
                      <div className="tab_content_button mt-40">
                        <div className="button">
                          <a href="#">Get A Quote<i className="fa fa-long-arrow-right" /></a>
                          <a className="active" href="#">Details View <i className="fa fa-long-arrow-right" /></a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-6">
                    <div className="tab_thumb">
                      <img src="assets/images/portfolio/tab2.jpg" alt />
                    </div>
                  </div>
                </div>
              </div>
              <div className="tab-pane mt-60" id="tabs-4" role="tabpanel">
                <div className="row">
                  <div className="col-lg-6">
                    <div className="tab_thumb">
                      <img src="assets/images/portfolio/tab3.jpg" alt />
                    </div>
                  </div>
                  <div className="col-lg-6">
                    <div className="tab_content ml-3">
                      <div className="tab_content_title pb-4">
                        <h4>Web Design &amp; Development</h4>
                      </div>
                      <div className="tab_content_text">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed eiusm tempor incididunt ut labore et dolore magna aliqua. Ut enim advis minim veniam, quis nostrud exercitat ullamco But I must explain to you how all this mistaken idea of denouncing</p>
                        <p>Porem tpsum dolor sit amet, consectetur adipisicing elit, sed eiusm morem wpsum solor sit amet, consectetur adipisicing elit, sed eiusm tempor incididunt ut labore et dolore magna</p>
                      </div>
                      <div className="tab_content_sub_text">
                        <ul>
                          <li> <i className="fa fa-long-arrow-right" /> Appropriately empower dynamic leadershp</li>
                          <li> <i className="fa fa-long-arrow-right" /> Globally myocardinate interactive supply chains</li>
                          <li> <i className="fa fa-long-arrow-right" /> Globally revolutionize global sources through</li>
                        </ul>
                      </div>
                      <div className="tab_content_button mt-40">
                        <div className="button">
                          <a href="#">Get A Quote<i className="fa fa-long-arrow-right" /></a>
                          <a className="active" href="#">Details View <i className="fa fa-long-arrow-right" /></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div className="flipbox_area choose_us pt-85 pb-70" style={{backgroundImage: 'url(assets/images/slider/slider-4.jpg)'}}>
    <div className="container">
      <div className="row">
        <div className="col-lg-12">
          <div className="section_title text_center white mb-55">
            <div className="section_sub_title uppercase mb-3">
              <h6>SERVICES</h6>
            </div>
            <div className="section_main_title">
              <h1>Provide Exclusive Services</h1>
            </div>
            <div className="em_bar">
              <div className="em_bar_bg" />
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-4 col-md-6 col-sm-12 col-xs-6">
          <div className="techno_flipbox mb-30">
            <div className="techno_flipbox_font" style={{backgroundImage: 'url(assets/images/galery/wh2.jpg)'}}>
              <div className="techno_flipbox_inner">
                <div className="techno_flipbox_icon mb-2">
                  <div className="icon">
                    <i className="flaticon-chart" />
                  </div>
                </div>			
                <div className="flipbox_title">
                  <h3>Deep Expertise &amp; Leadership</h3>
                </div>
              </div>
            </div>
            <div className="techno_flipbox_back" style={{backgroundImage: 'url(assets/images/galery/wh2.jpg)'}}>
              <div className="techno_flipbox_inner">
                <div className="techno_flipbox_icon mb-2">
                  <div className="icon">
                    <i className="flaticon-chart" />
                  </div>
                </div>			
                <div className="flipbox_title">
                  <h3>Deep Expertise &amp; Leadership</h3>
                </div>
                <div className="flipbox_desc">
                  <p>We have the technology and industry expertise to develop solutions that can connect people and businesses across a variety of mobile devices.</p>
                </div>
                <div className="flipbox_button">
                  <a href="#">Read More<i className="fa fa-angle-double-right" /></a>
                </div>
              </div>
            </div>
          </div>	
        </div>
        <div className="col-lg-4 col-md-6 col-sm-12 col-xs-6">
          <div className="techno_flipbox mb-30">
            <div className="techno_flipbox_font" style={{backgroundImage: 'url(assets/images/galery/wh1.jpg)'}}>
              <div className="techno_flipbox_inner">
                <div className="techno_flipbox_icon mb-2">
                  <div className="icon">
                    <i className="flaticon-analysis" />
                  </div>
                </div>			
                <div className="flipbox_title">
                  <h3>Cross-Industry Expertise</h3>
                </div>
              </div>
            </div>
            <div className="techno_flipbox_back" style={{backgroundImage: 'url(assets/images/galery/wh1.jpg)'}}>
              <div className="techno_flipbox_inner">
                <div className="techno_flipbox_icon mb-2">
                  <div className="icon">
                    <i className="flaticon-analysis" />
                  </div>
                </div>			
                <div className="flipbox_title">
                  <h3>Cross-Industry Expertise</h3>
                </div>
                <div className="flipbox_desc">
                  <p>We have the technology and industry expertise to develop solutions that can connect people and businesses across a variety of mobile devices.</p>
                </div>
                <div className="flipbox_button">
                  <a href="#">Read More<i className="fa fa-angle-double-right" /></a>
                </div>
              </div>
            </div>
          </div>	
        </div>
        <div className="col-lg-4 col-md-6 col-sm-12 col-xs-6">
          <div className="techno_flipbox mb-30">
            <div className="techno_flipbox_font" style={{backgroundImage: 'url(assets/images/galery/wh3.jpg)'}}>
              <div className="techno_flipbox_inner">
                <div className="techno_flipbox_icon mb-2">
                  <div className="icon">
                    <i className="flaticon-global-1" />
                  </div>
                </div>			
                <div className="flipbox_title">
                  <h3>Dedicated IT Solution</h3>
                </div>
              </div>
            </div>
            <div className="techno_flipbox_back" style={{backgroundImage: 'url(assets/images/galery/wh3.jpg)'}}>
              <div className="techno_flipbox_inner">
                <div className="techno_flipbox_icon mb-2">
                  <div className="icon">
                    <i className="flaticon-global-1" />
                  </div>
                </div>			
                <div className="flipbox_title">
                  <h3>Dedicated IT Solution</h3>
                </div>
                <div className="flipbox_desc">
                  <p>We have the technology and industry expertise to develop solutions that can connect people and businesses across a variety of mobile devices.</p>
                </div>
                <div className="flipbox_button">
                  <a href="#">Read More<i className="fa fa-angle-double-right" /></a>
                </div>
              </div>
            </div>
          </div>	
        </div>
      </div>
    </div>	
  </div>

  <div className="portfolio_area pt-80 pb-70" id="portfolio">
    <div className="container">
      <div className="row">
        {/* Start Section Tile */}
        <div className="col-lg-12">
          <div className="section_title text_center mb-50 mt-3">
            <div className="section_sub_title uppercase mb-3">
              <h6>PORTFOLIO</h6>
            </div>
            <div className="section_main_title">
              <h1>Our Latest Works For</h1>
              <h1>Your Business</h1>
            </div>
            <div className="em_bar">
              <div className="em_bar_bg" />
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-md-12">
          <div className="portfolio_nav">
            <div className="portfolio_menu">
              <ul className="menu-filtering">
                <li className="current_menu_item" data-filter="*">All Works</li>
                <li data-filter=".physics">Branding</li>
                <li data-filter=".cemes">Prototype</li>
                <li data-filter=".math">UX Research</li>
                <li data-filter=".english">Web Design</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div className="row image_load">
        <div className="col-lg-4 col-md-6 col-sm-12 grid-item physics english">
          <div className="single_portfolio">
            <div className="single_portfolio_inner">
              <div className="single_portfolio_thumb">
                <a href="#"><img src="assets/images/portfolio/p1.jpg" alt /></a>
              </div>
            </div>
            <div className="single_portfolio_content">
              <div className="single_portfolio_icon">
                <a className="portfolio-icon venobox vbox-item" data-gall="myportfolio" href="assets/images/portfolio/p1.jpg"><i className="fa fa-search-plus" /></a>
              </div>
              <div className="single_portfolio_content_inner">
                <span>Prototype UX Research</span>
                <h2><a href="#">Digital Marketing</a></h2>
              </div>
            </div>
          </div>
        </div>
        <div className="col-lg-4 col-md-6 col-sm-12 grid-item cemes">
          <div className="single_portfolio">
            <div className="single_portfolio_inner">
              <div className="single_portfolio_thumb">
                <a href="#"><img src="assets/images/portfolio/p2.jpg" alt /></a>
              </div>
            </div>
            <div className="single_portfolio_content">
              <div className="single_portfolio_icon">
                <a className="portfolio-icon venobox vbox-item" data-gall="myportfolio" href="assets/images/portfolio/p2.jpg"><i className="fa fa-search-plus" /></a>
              </div>
              <div className="single_portfolio_content_inner">
                <span>UX Research</span>
                <h2><a href="#">Awesome Creative</a></h2>
              </div>
            </div>
          </div>
        </div>
        <div className="col-lg-4 col-md-6 col-sm-12 grid-item cemes">
          <div className="single_portfolio">
            <div className="single_portfolio_inner">
              <div className="single_portfolio_thumb">
                <a href="#"><img src="assets/images/portfolio/p3.jpg" alt /></a>
              </div>
            </div>
            <div className="single_portfolio_content">
              <div className="single_portfolio_icon">
                <a className="portfolio-icon venobox vbox-item" data-gall="myportfolio" href="assets/images/portfolio/p3.jpg"><i className="fa fa-search-plus" /></a>
              </div>
              <div className="single_portfolio_content_inner">
                <span>Design, Photoshop</span>
                <h2><a href="#">Business Solution</a></h2>
              </div>
            </div>
          </div>
        </div>
        <div className="col-lg-4 col-md-6 col-sm-12 grid-item cemes math">
          <div className="single_portfolio">
            <div className="single_portfolio_inner">
              <div className="single_portfolio_thumb">
                <a href="#"><img src="assets/images/portfolio/p4.jpg" alt /></a>
              </div>
            </div>
            <div className="single_portfolio_content">
              <div className="single_portfolio_icon">
                <a className="portfolio-icon venobox vbox-item" data-gall="myportfolio" href="assets/images/portfolio/p4.jpg"><i className="fa fa-search-plus" /></a>
              </div>
              <div className="single_portfolio_content_inner">
                <span>Software Engineer</span>
                <h2><a href="#">Company Project</a></h2>
              </div>
            </div>
          </div>
        </div>
        <div className="col-lg-4 col-md-12 col-sm-12 grid-item physics english">
          <div className="single_portfolio admin">
            <div className="single_portfolio_inner">
              <div className="single_portfolio_thumb">
                <a href="#"><img src="assets/images/portfolio/p5.jpg" alt /></a>
              </div>
            </div>
            <div className="single_portfolio_content">
              <div className="single_portfolio_icon">
                <a className="portfolio-icon venobox vbox-item" data-gall="myportfolio" href="assets/images/portfolio/p5.jpg"><i className="fa fa-search-plus" /></a>
              </div>
              <div className="single_portfolio_content_inner">
                <span>Photoshop</span>
                <h2><a href="#">Mastering Web Design</a></h2>
              </div>
            </div>
          </div>
        </div>
        <div className="col-lg-4 col-md-12 col-sm-12 grid-item math">
          <div className="single_portfolio admin">
            <div className="single_portfolio_inner">
              <div className="single_portfolio_thumb">
                <a href="#"><img src="assets/images/portfolio/p6.jpg" alt /></a>
              </div>
            </div>
            <div className="single_portfolio_content">
              <div className="single_portfolio_icon">
                <a className="portfolio-icon venobox vbox-item" data-gall="myportfolio" href="assets/images/portfolio/p6.jpg"><i className="fa fa-search-plus" /></a>
              </div>
              <div className="single_portfolio_content_inner">
                <span>Design, Photoshop</span>
                <h2><a href="#">Awesome Logo Design</a></h2>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div className="contact_area pt-85 pb-90" style={{backgroundImage: 'url(assets/images/bg-cnt.jpg)'}}>
    <div className="container">
      <div className="row">
        <div className="col-lg-12">
          <div className="section_title white text_center mb-60 mt-3">
            <div className="section_sub_title uppercase mb-3">
              <h6>GET QUOTE</h6>
            </div>
            <div className="section_main_title">
              <h1>Make An</h1>
              <h1>Free Consultant</h1>
            </div>
            <div className="em_bar">
              <div className="em_bar_bg" />
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-xl-12">
          <div className="quote_wrapper">
            <form id="contact_form" action="http://html.dreamitsolution.net/techno/mail.php" method="POST">
              <div className="row">
                <div className="col-lg-6">
                  <div className="form_box mb-30">
                    <input type="text" name="name" placeholder="Name" />
                  </div>
                </div>
                <div className="col-lg-6">
                  <div className="form_box mb-30">
                    <input type="email" name="email" placeholder="Email Address" />
                  </div>
                </div>
                <div className="col-lg-6">
                  <div className="form_box mb-30">
                    <input type="text" name="phone" placeholder="Phone Number" />
                  </div>
                </div>
                <div className="col-lg-6">
                  <div className="form_box mb-30">
                    <input type="text" name="web" placeholder="Website" />
                  </div>
                </div>
                <div className="col-lg-12">
                  <div className="form_box mb-30">
                    <textarea name="message" id="message" cols={30} rows={10} placeholder="Write a Message" defaultValue={""} />
                  </div>
                  <div className="quote_btn text_center">
                    <button className="btn" type="submit">Free Consultancy</button>
                  </div>
                </div>
              </div>
            </form>
            <p className="form-message" />
          </div>
        </div>
      </div>
    </div>
  </div>

  <div className="team_area bg_color2 pt-80 pb-75">
    <div className="container">
      <div className="row">
        {/* Start Section Tile */}
        <div className="col-lg-12">
          <div className="section_title text_center mb-50 mt-3">
            <div className="section_sub_title uppercase mb-3">
              <h6>TEAM MEMBER</h6>
            </div>
            <div className="section_main_title">
              <h1>Our Awesome Creative</h1>
              <h1>Team Member</h1>
            </div>
            <div className="em_bar">
              <div className="em_bar_bg" />
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-4 col-md-6 col-sm-12">
          <div className="single_team mb-4">
            <div className="single_team_thumb">
              <img src="assets/images/team1.jpg" alt />
              <div className="single_team_icon">
                <a href="#"><i className="fa fa-facebook" /></a>
                <a href="#"><i className="fa fa-twitter" /></a>
                <a href="#"><i className="fa fa-linkedin" /></a>
                <a href="#"><i className="fa fa-pinterest" /></a>
              </div>
            </div>
            <div className="single_team_content">
              <h4>David Malaan</h4>
              <span>CEO</span>
            </div>
          </div>
        </div>
        <div className="col-lg-4 col-md-6 col-sm-12">
          <div className="single_team mb-4">
            <div className="single_team_thumb">
              <img src="assets/images/team2.jpg" alt />
              <div className="single_team_icon">
                <a href="#"><i className="fa fa-facebook" /></a>
                <a href="#"><i className="fa fa-twitter" /></a>
                <a href="#"><i className="fa fa-linkedin" /></a>
                <a href="#"><i className="fa fa-pinterest" /></a>
              </div>
            </div>
            <div className="single_team_content">
              <h4>Andres Jhohne</h4>
              <span>DIRECTOR</span>
            </div>
          </div>
        </div>
        <div className="col-lg-4 col-md-6 col-sm-12">
          <div className="single_team mb-4">
            <div className="single_team_thumb">
              <img src="assets/images/team3.jpg" alt />
              <div className="single_team_icon">
                <a href="#"><i className="fa fa-facebook" /></a>
                <a href="#"><i className="fa fa-twitter" /></a>
                <a href="#"><i className="fa fa-linkedin" /></a>
                <a href="#"><i className="fa fa-pinterest" /></a>
              </div>
            </div>
            <div className="single_team_content">
              <h4>Michel Balak</h4>
              <span>FOUNDER</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div className="call_do_action pt-85 pb-130 bg_color" style={{backgroundImage: 'url(assets/images/call-bg.png)'}}>
    <div className="container">
      <div className="row">
        <div className="col-lg-12">
          <div className="section_title white text_center mb-60 mt-3">
            <div className="phone_number mb-3">
              <h5>+880 013 143 206</h5>
            </div>
            <div className="section_main_title">
              <h1>To make requests for the</h1>
              <h1>further information</h1>
            </div>
            <div className="button three mt-40">
              <a href="#">Join With Now<i className="fa fa-long-arrow-right" /></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div className="counter_area">
    <div className="container">
      <div className="row cntr_bg_up nagative_margin pt-50 pb-45">
        <div className="col-lg-3 col-md-6 col-sm-12">
          <div className="single_counter text_center mb-4">
            <div className="countr_text">
              <h1><span className="counter">15</span><span>K</span> </h1>
            </div>
            <div className="counter_desc">
              <h5>Happy Clients</h5>
            </div>
          </div>
        </div>
        <div className="col-lg-3 col-md-6 col-sm-12">
          <div className="single_counter text_center mb-4">
            <div className="countr_text">
              <h1><span className="counter">1280</span><span>+</span> </h1>
            </div>
            <div className="counter_desc">
              <h5>Account Number</h5>
            </div>
          </div>
        </div>
        <div className="col-lg-3 col-md-6 col-sm-12">
          <div className="single_counter text_center mb-4">
            <div className="countr_text">
              <h1><span className="counter">10</span><span>K</span> </h1>
            </div>
            <div className="counter_desc">
              <h5>Finished Projects</h5>
            </div>
          </div>
        </div>
        <div className="col-lg-3 col-md-6 col-sm-12">
          <div className="single_counter text_center mb-4">
            <div className="countr_text">
              <h1><span className="counter">992</span><span>+</span> </h1>
            </div>
            <div className="counter_desc">
              <h5>Win Awards</h5>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div className="blog_area pt-85 pb-65">
    <div className="container">
      <div className="row">
        <div className="col-lg-12">
          <div className="section_title text_center mb-60 mt-3">
            <div className="section_sub_title uppercase mb-3">
              <h6>LATEST ARTICLE</h6>
            </div>
            <div className="section_main_title">
              <h1>Our Latest Blog Posts</h1>
            </div>
            <div className="em_bar">
              <div className="em_bar_bg" />
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-4 col-md-6 col-sm-12">
          <div className="single_blog mb-4">
            <div className="single_blog_thumb pb-4">
              <a href="blog-details.html"><img src="assets/images/blog1.jpg" alt /></a>
            </div>
            <div className="single_blog_content pl-4 pr-4">
              <div className="techno_blog_meta">
                <a href="#">Techno </a>
                <span className="meta-date pl-3">January 3, 2020</span>
              </div>
              <div className="blog_page_title pb-1">
                <h3><a href="blog-details.html">The five devices you need to work anytime</a></h3>
              </div>
              <div className="blog_description">
                <p>Lorem ipsum dolor sit amet consectet adipisie cing elit sed eiusmod tempor incididunt on labore et dolore.</p>
              </div>
              <div className="blog_page_button pb-4">
                <a href="blog-details.html">Read More <i className="fa fa-long-arrow-right" /></a>
              </div>
            </div>
          </div>
        </div>
        <div className="col-lg-4 col-md-6 col-sm-12">
          <div className="single_blog mb-4">
            <div className="single_blog_thumb pb-4">
              <a href="blog-details.html"><img src="assets/images/blog2.jpg" alt /></a>
            </div>
            <div className="single_blog_content pl-4 pr-4">
              <div className="techno_blog_meta">
                <a href="#">Techno </a>
                <span className="meta-date pl-3">December 3, 2020</span>
              </div>
              <div className="blog_page_title pb-1">
                <h3><a href="blog-details.html">How to learn PHP 10 tips to get you started</a></h3>
              </div>
              <div className="blog_description">
                <p>Lorem ipsum dolor sit amet consectet adipisie cing elit sed eiusmod tempor incididunt on labore et dolore.</p>
              </div>
              <div className="blog_page_button pb-4">
                <a href="blog-details.html">Read More <i className="fa fa-long-arrow-right" /></a>
              </div>
            </div>
          </div>
        </div>
        <div className="col-lg-4 col-md-6 col-sm-12">
          <div className="single_blog mb-4">
            <div className="single_blog_thumb pb-4">
              <a href="blog-details.html"><img src="assets/images/blog3.jpg" alt /></a>
            </div>
            <div className="single_blog_content pl-4 pr-4">
              <div className="techno_blog_meta">
                <a href="#">Techno </a>
                <span className="meta-date pl-3">Augost 5, 2020</span>
              </div>
              <div className="blog_page_title pb-1">
                <h3><a href="blog-details.html">The five devices you need to work anytime</a></h3>
              </div>
              <div className="blog_description">
                <p>Lorem ipsum dolor sit amet consectet adipisie cing elit sed eiusmod tempor incididunt on labore et dolore.</p>
              </div>
              <div className="blog_page_button pb-4">
                <a href="blog-details.html">Read More <i className="fa fa-long-arrow-right" /></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div className="subscribe_area style_two">
    <div className="container">
      <div className="row sbc_bg_box">
        <div className="col-lg-1" />
        <div className="col-lg-10">
          <div className="subscribe_bg_box">
            <div className="single_subscribe_contact">
              <div className="subscribe_content_title white text_center pb-30">
                <h2>Subscribe Our Newsletter</h2>
              </div>
              <form action="#">
                <div className="subscribe_form">
                  <input type="email" name="email" id="email" className="form-control" required data-error="Please enter your email" placeholder="Enter Your Email" />
                  <div className="help-block with-errors" />
                </div>
                <div className="subscribe_form_send">
                  <button type="submit" className="btn">
                    Subscribe
                  </button>
                  <div id="msgSubmit" className="h3 text-center hidden" />
                  <div className="clearfix" />
                </div>
              </form>
            </div>
          </div>
        </div>
        <div className="col-lg-1" />
      </div>
    </div>
  </div>

  <div className="footer-middle pt-250"> 
    <div className="container">
      <div className="row">
        <div className="col-lg-3 col-md-6 col-sm-12">
          <div className="widget widgets-company-info">
            <div className="footer-bottom-logo pb-40">
              <img src="assets/images/logo.png" alt />
            </div>
            <div className="company-info-desc">
              <p>Condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus.
              </p>
            </div>
            <div className="follow-company-info pt-3">
              <div className="follow-company-text mr-3">
                <a href="#"><p>Follow Us</p></a>
              </div>
              <div className="follow-company-icon">
                <a href="#"><i className="fa fa-facebook" /></a>
                <a href="#"><i className="fa fa-twitter" /></a>
                <a href="#"><i className="fa fa-linkedin" /></a>
                <a href="#"><i className="fa fa-skype" /></a>
              </div>
            </div>
          </div>					
        </div>
        <div className="col-lg-3 col-md-6 col-sm-12">
          <div className="widget widget-nav-menu">
            <h4 className="widget-title pb-4">Our Services</h4>
            <div className="menu-quick-link-container ml-4">
              <ul id="menu-quick-link" className="menu">
                <li><a href="#">Marketing Strategy</a></li>
                <li><a href="#">Interior Design</a></li>
                <li><a href="#">Digital Services</a></li>
                <li><a href="#">Product Selling</a></li>
                <li><a href="#">Product Design</a></li>
                <li><a href="#">Social Marketing</a></li>
              </ul>
            </div>
          </div>
        </div>	
        <div className="col-lg-3 col-md-6 col-sm-12">
          <div className="widget widgets-company-info">
            <h3 className="widget-title pb-4">Company Address</h3>
            <div className="company-info-desc">
              <p>Porem awesome dolor sitework amet, consetur acing elit, sed do eiusmod ligal
              </p>
            </div>	
            <div className="footer-social-info">
              <p><span>Address :</span>54/1 New dhas sorini Asut, Melbord Austria.</p>
            </div>
            <div className="footer-social-info">
              <p><span>Phone :</span>54786547521</p>
            </div>
            <div className="footer-social-info">
              <p><span>Email :</span>demo@example.com</p>
            </div>
          </div>					
        </div>
        <div className="col-lg-3 col-md-6 col-sm-12">
          <div id="em-recent-post-widget">
            <div className="single-widget-item">
              <h4 className="widget-title pb-3">Popular Post</h4>				
              <div className="recent-post-item active pb-3">
                <div className="recent-post-image mr-3">
                  <a href="#">
                    <img width={80} height={80} src="assets/images/recent1.jpg" alt />					
                  </a>
                </div>
                <div className="recent-post-text">
                  <h6><a href="#">
                      Tiktok Illegally collecting data sharing										
                    </a>
                  </h6>					
                  <span className="rcomment">December 12, 2020</span>
                </div>
              </div>
              <div className="recent-post-item pt-1">
                <div className="recent-post-image mr-3">
                  <a href="#">
                    <img width={80} height={80} src="assets/images/recent3.jpg" alt />					
                  </a>
                </div>
                <div className="recent-post-text">
                  <h6><a href="#">
                      How can use our latest news by									
                    </a>
                  </h6>					
                  <span className="rcomment">December 15, 2020</span>
                </div>
              </div>
            </div>
          </div>	
        </div>
      </div>
      <div className="row footer-bottom mt-70 pt-3 pb-1">
        <div className="col-lg-6 col-md-6">
          <div className="footer-bottom-content">
            <div className="footer-bottom-content-copy">
              <p>© 2020 Techno.All Rights Reserved. </p>
            </div>
          </div>
        </div>
        <div className="col-lg-6 col-md-6">
          <div className="footer-bottom-right">
            <div className="footer-bottom-right-text">
              <a className="absod" href="#">Privacy Policy </a>
              <a href="#"> Terms &amp; Conditions</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
  </React.Fragment>

  );
}

export default App;
