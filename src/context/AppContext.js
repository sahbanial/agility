import React from "react";
import data from "config/data";
const AppContext = React.createContext();
const AppContextProvider = (props) => {
  return (
    <AppContext.Provider
      value={{
        data,
      }}
    >
      {props.children}
    </AppContext.Provider>
  );
};
const useAppContext = () => React.useContext(AppContext);
export { AppContextProvider, useAppContext };
