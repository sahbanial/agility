import About from "pages/About";
import Contact from "pages/Contact";
import Partner from "pages/Partner";
import Service from "pages/Service";
import React, { lazy, Suspense } from "react";
import { Route, Router, Switch } from "react-router-dom";
import { history } from "./history";
import Home from "./pages/Home";
const publicRoutes = [
  {
    path: "/",
    component: Home,
    exact: true,
  },
  {
    path: "/home",
    component: Home,
    exact: true,
  },
  {
    path: "/company",
    component: About,
    exact: true,
  },
  {
    path: "/service",
    component: Service,
    exact: true,
  },
  {
    path: "/partners",
    component: Partner,
    exact: true,
  },
  {
    path: "/contact",
    component: Contact,
    exact: true,
  },
];

const AppRoute = ({
  component: Component,
  fullLayout,
  permission,
  user,
  publicRoute,
  ...rest
}) => (
  <Route
    {...rest}
    render={(props) => {
      return <Component {...props} />;
    }}
  />
);

const AppRouter = () => {
  return (
    <Router history={history}>
      <Switch>
        {publicRoutes.map((route) => (
          <AppRoute {...route} />
        ))}
      </Switch>
    </Router>
  );
};
export default AppRouter;
