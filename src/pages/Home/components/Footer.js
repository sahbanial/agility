import { useAppContext } from "context/AppContext";
import React from "react";

export default function Footer() {
  const { data } = useAppContext();
  return (
    <div className="footer-middle pt-3">
      <div className="container">
        <div className="row">
          <div className="col-lg-3 col-md-6 col-sm-12">
            <div className="widget widgets-company-info">
              <h3 className="widget-title pb-4">Adresse d'entreprise</h3>

              <div className="footer-social-info">
                <p>
                  <span>Adresse :</span>
                  {data?.address}
                </p>
              </div>
              <div className="footer-social-info">
                <p>
                  <span>Tél :</span> {data?.tel}
                </p>
              </div>
              <div className="footer-social-info">
                <p>
                  <span>Email :</span>
                  {data?.email}
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="row footer-bottom mt-70 pt-3 pb-1">
          <div className="col-lg-6 col-md-6">
            <div className="footer-bottom-content">
              <div className="footer-bottom-content-copy">
                <p>© 2021 Agility </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
