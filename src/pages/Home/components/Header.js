import { Images } from "../../../config";
import React from "react";
import data from "config/data";

export default function Header() {
  return (
    <div>
      <div
        id="sticky-header"
        className="techno_nav_manu transparent_menu white d-md-none d-lg-block d-sm-none d-none"
      >
        <div className="container">
          <div className="row">
            <div className="col-md-3">
              <div className="logo mt-4">
                <a className="logo_img" href="/" title="techno">
                  <img src={Images.logo} alt className="logo-home" />
                </a>
                <a className="main_sticky" href="/" title="techno">
                  <img src={Images.logo} alt="astute" />
                </a>
              </div>
            </div>
            <div className="col-md-9">
              <nav className="techno_menu">
                <ul className="nav_scroll">
                  <li>
                    <a href="/home">{data?.menu.home}</a>
                  </li>
                  <li>
                    <a href="/company">{data?.menu.company}</a>
                  </li>
                  <li>
                    <a href="/service">{data?.menu.services}</a>
                  </li>
                  <li>
                    <a href="/partners">{data?.menu.partner}</a>
                  </li>

                  <li>
                    <a href="/contact">{data?.menu.contact}</a>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
