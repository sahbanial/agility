import { Images } from "../../../config";
import React from "react";
import classNames from "classnames";
import data from "config/data";
export default function DigitalMarketing() {
  const [activeTab, setActiveTab] = React.useState(0);
  function changeTab(tab) {
    setActiveTab(tab);
  }
  return (
    <div>
      <div className="tab_area pb-100">
        <div className="container">
          <div className="row nagative_margin2">
            <div className="col-lg-12">
              <div className="tab_content">
                <ul className="nav nav-tabs" role="tablist">
                  <li className="nav-item">
                    <a
                      className={classNames(
                        "nav-link",
                        activeTab == 0 && "active"
                      )}
                      data-toggle="tab"
                      onClick={() => setActiveTab(0)}
                      role="tab"
                      style={{ fontSize: "15px" }}
                    >
                      {" "}
                      <i className="fa fa-laptop" />{" "}
                      {data?.home?.digitalMarketing}
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      className={classNames(
                        "nav-link",
                        activeTab == 1 && "active"
                      )}
                      data-toggle="tab"
                      onClick={() => setActiveTab(1)}
                      role="tab"
                      style={{ fontSize: "15px" }}
                    >
                      <i className="fa fa-laptop" /> {data?.home?.webHosting}
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      className={classNames(
                        "nav-link",
                        activeTab == 2 && "active"
                      )}
                      data-toggle="tab"
                      onClick={() => setActiveTab(2)}
                      role="tab"
                      style={{ fontSize: "15px" }}
                    >
                      <i className="fa fa-database" />{" "}
                      {data?.home?.cyberSecurity}
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      className={classNames(
                        "nav-link",
                        activeTab == 3 && "active"
                      )}
                      data-toggle="tab"
                      onClick={() => setActiveTab(3)}
                      role="tab"
                      style={{ fontSize: "15px" }}
                    >
                      <i className="fa fa-html5" /> {data?.home?.smartCamera}
                    </a>
                  </li>
                </ul>
                {/* Tab panes */}
                <div className="tab-content pt-3">
                  {activeTab == 0 && (
                    <div className="tab-pane active mt-60">
                      <div className="row">
                        <div className="col-lg-6">
                          <div className="tab_thumb">
                            <img src={Images.tab2} alt />
                          </div>
                        </div>
                        <div className="col-lg-6">
                          <div className="tab_content ml-3">
                            <div className="tab_content_title pb-4">
                              <h4>{data?.home?.digitalMarketing}</h4>
                            </div>
                            <div className="tab_content_text">
                              <p>{data?.home?.digitalMarketingDescription}</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  )}

                  {/** */}
                  {activeTab == 1 && (
                    <div className="mt-60">
                      <div className="row">
                        <div className="col-lg-6">
                          <div className="tab_thumb">
                            <img src={Images.tab1} alt />
                          </div>
                        </div>
                        <div className="col-lg-6">
                          <div className="tab_content ml-3">
                            <div className="tab_content_title pb-4">
                              <h4>{data?.home?.webHostingTitle}</h4>
                            </div>
                            <div className="tab_content_text">
                              <p>{data?.home?.webHostingDescription}</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  )}

                  {/*** */}
                  {activeTab == 2 && (
                    <div className="mt-60" id="tabs-3" role="tabpanel">
                      <div className="row">
                        <div className="col-lg-6">
                          <div className="tab_content">
                            <div className="tab_content_title pb-4">
                              <h4>{data?.home?.cyberSecurityTitle}</h4>
                            </div>
                            <div className="tab_content_text">
                              <p>{data?.home?.cyberSecurityDescription}</p>
                            </div>
                          </div>
                        </div>
                        <div className="col-lg-6">
                          <div className="tab_thumb">
                            <img src={Images.tab2} alt />
                          </div>
                        </div>
                      </div>
                    </div>
                  )}
                  {/*** */}
                  {activeTab == 3 && (
                    <div className="mt-60" id="tabs-4" role="tabpanel">
                      <div className="row">
                        <div className="col-lg-6">
                          <div className="tab_thumb">
                            <img src={Images.tab3} alt />
                          </div>
                        </div>
                        <div className="col-lg-6">
                          <div className="tab_content ml-3">
                            <div className="tab_content_title pb-4">
                              <h4>{data?.home?.smartCameraTitle} </h4>
                            </div>
                            <div className="tab_content_text">
                              <p>{data?.home?.smartCamerDescription}</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
