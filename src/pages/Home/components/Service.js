import { Images } from "config";
import data from "config/data";
import React from "react";

export default function Service() {
  return (
    <div>
      <div className="tab_top_area pt-100 pb-150 service-background">
        <div className="container">
          <div className="row ">
            <div className="col-lg-12">
              <div className="section_title text_center white mb-55">
                <div className="section_sub_title uppercase mb-3">
                  <h6>{data?.home?.services}</h6>
                </div>
                <div className="section_main_title">
                  <h1>{data?.home?.ourServices}</h1>
                </div>
                <div className="em_bar">
                  <div className="em_bar_bg" />
                </div>
                <div className="section_content_text pt-4">
                  <p>{data?.home?.aboutUsSubDescription}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
