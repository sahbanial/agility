import { Images } from "../../../config";
import React from "react";
import data from "config/data";

export default function About() {
  return (
    <div>
      <div className="about_area pt-70 pb-70">
        <div className="container">
          <div className="row">
            <div className="col-lg-6 col-md-6 col-sm-12 col-xs-6">
              <div className="section_title text_left mb-40 mt-3">
                <div className="section_sub_title uppercase mb-3">
                  <h6>{data?.home.aboutUs}</h6>
                </div>
                <div className="section_main_title">
                  <h1>{data?.home?.aboutUsTitle}</h1>
                  <h1>
                    {data?.home?.aboutUsTitle2}
                    <span>IT Solutions.</span>
                  </h1>
                </div>
                <div className="em_bar">
                  <div className="em_bar_bg" />
                </div>
                <div className="section_content_text bold pt-5">
                  <p>{data?.home?.aboutUsDescription}</p>
                </div>
              </div>
              <div className="singel_about_left mb-30">
                <div className="singel_about_left_inner mb-3">
                  <div className="singel-about-content boder pl-4">
                    <p>{data?.home?.aboutUsSubDescription}</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-6 col-md-6 col-sm-12 col-xs-6">
              <div className="single_video">
                <div className="video_thumb text_center mt-160 ml-100">
                  <img src={Images.video1} alt />
                  <div className="main_video">
                    <div className="video-icon">
                      <a
                        className="video-vemo-icon venobox vbox-item"
                        data-vbtype="youtube"
                        data-autoplay="true"
                        href="https://youtu.be/BS4TUd7FJSg"
                      >
                        <i className="fa fa-play" />
                      </a>
                    </div>
                  </div>
                </div>
                <div className="video_shape">
                  <div className="video_shape_thumb1 bounce-animate">
                    <img
                      src={Images.vp1}
                      alt
                      style={{ height: "500px", width: "500px" }}
                    />
                  </div>
                </div>
                <div className="video_shape">
                  <div className="video_shape_thumb2 bounce-animate">
                    <img src={Images.vp2} alt />
                  </div>
                </div>
                <div className="video_shape">
                  <div className="video_shape_thumb3 bounce-animate">
                    <img src={Images.vp3} alt />
                  </div>
                </div>
                <div className="video_shape">
                  <div className="video_shape_thumb4 bounce-animate">
                    <img src={Images.vp4} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
