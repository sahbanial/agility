import { Images } from "../../../config";
import React from "react";
import data from "config/data";

export default function Slider() {
  return (
    <div>
      <div className="slider_area d-flex align-items-center slider3" id="home">
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="single_slider">
                <div className="slider_content">
                  <div className="slider_text">
                    <div className="slider_text_inner">
                      <h1> {data.home.title} </h1>
                      <h1>{data.home.title2}</h1>
                    </div>
                    <div className="slider_text_desc pt-4">
                      <p>{data?.home?.subTitle} </p>
                    </div>
                    <div className="slider_button pt-5 d-flex"></div>
                    <div className="slider-video two mt-50">
                      <div className="video-icon">
                        <a
                          className="video-vemo-icon venobox vbox-item"
                          data-vbtype="youtube"
                          data-autoplay="true"
                        >
                          <i className="fa fa-play" />
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="single_slider_shape">
                  <div className="single_slider_shape_image">
                    <img src={Images.shape1} alt />
                  </div>
                </div>
                <div className="single_slider_rot">
                  <div className="single_slider_rot_inner rotateme">
                    <img src={Images.sdt} alt />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
