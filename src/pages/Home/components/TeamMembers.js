import React from 'react'

export default function TeamMembers() {
    return (
        <div className="team_area bg_color2 pt-80 pb-75">
    <div className="container">
      <div className="row">
        {/* Start Section Tile */}
        <div className="col-lg-12">
          <div className="section_title text_center mb-50 mt-3">
            <div className="section_sub_title uppercase mb-3">
              <h6>TEAM MEMBER</h6>
            </div>
            <div className="section_main_title">
              <h1>Our Awesome Creative</h1>
              <h1>Team Member</h1>
            </div>
            <div className="em_bar">
              <div className="em_bar_bg" />
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-4 col-md-6 col-sm-12">
          <div className="single_team mb-4">
            <div className="single_team_thumb">
              <img src="assets/images/team1.jpg" alt />
             
            </div>
            <div className="single_team_content">
              <h4>David Malaan</h4>
              <span>CEO</span>
            </div>
          </div>
        </div>
        <div className="col-lg-4 col-md-6 col-sm-12">
          <div className="single_team mb-4">
            <div className="single_team_thumb">
              <img src="assets/images/team2.jpg" alt />
              <div className="single_team_icon">
                <a href="#"><i className="fa fa-facebook" /></a>
                <a href="#"><i className="fa fa-twitter" /></a>
                <a href="#"><i className="fa fa-linkedin" /></a>
                <a href="#"><i className="fa fa-pinterest" /></a>
              </div>
            </div>
            <div className="single_team_content">
              <h4>Andres Jhohne</h4>
              <span>DIRECTOR</span>
            </div>
          </div>
        </div>
        <div className="col-lg-4 col-md-6 col-sm-12">
          <div className="single_team mb-4">
            <div className="single_team_thumb">
              <img src="assets/images/team3.jpg" alt />
            </div>
            <div className="single_team_content">
              <h4>Michel Balak</h4>
              <span>FOUNDER</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
    )
}
