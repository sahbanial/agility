import React from 'react';
import Header from './components/Header';
import Slider from './components/Slider';
import About from './components/About';
import Service from './components/Service';
import DigitalMarketing from './components/DigitalMarketing';
import TeamMembers from './components/TeamMembers';
import Footer from './components/Footer';
function App() {
	return (
		<React.Fragment>
			<Header />
			<Slider />
			<About />
			<Service />
			<DigitalMarketing />
			{/* <TeamMembers /> */}
			<Footer />
		</React.Fragment>
	);
}

export default App;
