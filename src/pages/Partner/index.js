import Images from "config/Images";
import Layout from "layout/Layout";
import React from "react";

export default function Partner() {
  const partners = [
    {
      image: Images.partner.safeness,
      title: "Safeness",
      description: "",
      url: "http://safeness.com.tn",
    },
    {
      image: Images.partner.sigma,
      title: "Sigma",
      description: "",
      url: "http://www.sigma-energy.com/",
    },
    {
      image: Images.partner.preventis,
      title: "preventis",
      description: "",
      url: "https://www.preventis.com.tn/",
    },
  ];
  return (
    <Layout pageTitle="Nos partenaires">
      <div className="row justify-content-center">
        {partners.map((par) => (
          <div className="col-md-3 col-sm-12 col-lg-3 d-flex justify-content-center p-4 ">
            <div className="col-lg-12">
              <div className="single_portfolio">
                <div className="single_portfolio_inner">
                  <div className="single_portfolio_thumb">
                    <a href={par.url}>
                      <img src={par.image} alt="" />
                    </a>
                  </div>
                </div>
                <div className="single_portfolio_content">
                  <div className="single_portfolio_icon">
                    <a
                      className="portfolio-icon venobox vbox-item"
                      data-gall="myportfolio"
                    >
                      <i class="fa fa-search-plus"></i>
                    </a>
                  </div>
                  <div className="single_portfolio_content_inner">
                    <span>{par?.title}</span>
                    <h2>
                      <a href="portfolio-details.html">{par.description}</a>
                    </h2>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
    </Layout>
  );
}
