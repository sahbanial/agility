import Layout from "layout/Layout";
import React from "react";
import { UilSitemap } from "@iconscout/react-unicons";
import data from "config/data";
export default function Service() {
  return (
    <Layout pageTitle="Nos services">
      <div className="service_area bg_color2 pt-85 pb-75">
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="section_title text_center mb-55">
                <div className="section_sub_title uppercase mb-3">
                  <h6>SERVICES</h6>
                </div>
                <div className="section_main_title">
                  <h1>Service que nous fournissons</h1>
                </div>
                <div className="em_bar">
                  <div className="em_bar_bg" />
                </div>
                <div className="section_content_text pt-4">
                  <p>
                    Nous maîtrisons tout le cycle de vie de votre projet, depuis
                    infrastructures réseau aux applications, avec l'ambition
                    d'atteindre vos objectifs commerciaux et d'optimiser la
                    performance de votre entreprise. Nous concevons, intégrons
                    et gérons une large gamme de solutions à haute valeur
                    ajoutée et de qualité prestations de service. Nous
                    développons constamment les compétences de nos équipes, pour
                    proposer des solutions innovantes et évolutives.
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-4 col-md-6 col-sm-12">
              <div className="service_style_one text_center pt-40 pb-40 pl-3 pr-3 mb-4">
                <div className="service_style_one_title mb-30">
                  <h4>Architecture informatique, intégration et déploiement</h4>
                </div>
                <div className="service_style_one_text">
                  <p>
                    Agility vous accompagne tout au long du cycle de vie de
                    votre projets: Analysez vos besoins, émettez des
                    recommandations, selon les tendances technologiques,
                    concevoir, intégrer et déployer des solutions et des
                    architectures en respectant au mieux pratiques et
                    méthodologies éprouvées.
                  </p>
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-md-6 col-sm-12">
              <div className="service_style_one text_center pt-40 pb-40 pl-3 pr-3 mb-4">
                <div className="service_style_one_title mb-30">
                  <h4>Conseil et stratégie informatique</h4>
                </div>
                <div className="service_style_one_text">
                  <p>
                    ONos experts vous aident à vous réorganiser aujourd'hui pour
                    réussir demain. Des centaines de projets informatiques
                    réussis sont à notre honneur. Nous vous offrir des conseils,
                    un accompagnement et une meilleure stratégie informatique,
                    tout converge vers votre objectif professionnel.
                  </p>
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-md-6 col-sm-12">
              <div className="service_style_one text_center pt-40 pb-40 pl-3 pr-3 mb-4">
                <div className="service_style_one_title mb-30">
                  <h4>Assistance et maintenance</h4>
                </div>
                <div className="service_style_one_text">
                  <p>
                    Agility dédie une structure à la relation client gestion, en
                    vue d'une meilleure expérience client. Son mission est de
                    fournir un soutien continu et préventif et entretien
                    curatif.
                  </p>
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-md-6 col-sm-12">
              <div className="service_style_one text_center pt-40 pb-40 pl-3 pr-3 mb-4">
                <div className="service_style_one_title mb-30">
                  <h4>Formation et transfert de compétences</h4>
                </div>
                <div className="service_style_one_text">
                  <p>
                    Le savoir-faire de nos experts vous permet de réussir votre
                    projets et améliorez votre ROI. Formation certifiante en
                    informatique et la gouvernance est assurée par notre centre
                    de formation Advancia Formation.
                  </p>
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-md-6 col-sm-12">
              <div className="service_style_one text_center pt-40 pb-40 pl-3 pr-3 mb-4">
                <div className="service_style_one_title mb-30">
                  <h4>Hébergement web</h4>
                </div>
                <div className="service_style_one_text">
                  <p>
                    Vous êtes-vous déjà demandé: «Qu'est-ce que l'hébergement
                    Web?» Si vous possédez un domaine et souhaitez le partager
                    avec le monde entier, vous avez besoin du Web services
                    d'hébergement. Chaque site Web a besoin d'une «maison» en
                    ligne, - un endroit où les données peuvent être stockées et
                    un moyen de stocker ces données en ligne via un fournisseur
                    d'hébergement Web
                  </p>
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-md-6 col-sm-12">
              <div className="service_style_one text_center pt-40 pb-40 pl-3 pr-3 mb-4">
                <div className="service_style_one_title mb-30">
                  <h4>Le marketing numérique</h4>
                </div>
                <div className="service_style_one_text">
                  <p>
                    Le marketing numérique fait référence à toutes les
                    techniques de marketing utilisées sur médias et canaux
                    numériques pour promouvoir les produits et services. En
                    marketing digital, une notion essentielle émerge,
                    l'interactivité. On peut aussi parler de marketing web
                    depuis la différence est principalement liée à
                    l'environnement, ici l'Internet
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="flipbox_area top_feature pb-70 two">
        <div className="container">
          <div className="row">
            <div className="col-lg-3 col-md-6 col-sm-12 col-xs-6">
              <div className="techno_flipbox mb-30">
                <div className="techno_flipbox_font">
                  <div className="techno_flipbox_inner">
                    <div className="techno_flipbox_icon">
                      <div className="icon">
                        <i className="flaticon-code" />
                      </div>
                    </div>
                    <div className="flipbox_title">
                      <h3>{data?.company?.services?.responsiveDesign}</h3>
                    </div>
                  </div>
                </div>
                <div className="techno_flipbox_back">
                  <div className="techno_flipbox_inner">
                    <div className="flipbox_title">
                      <h3>{data?.company?.services?.responsiveDesign}</h3>
                    </div>
                    <div className="flipbox_desc pt-3">
                      <p>
                        {data?.company?.services?.responsiveDesginDescriptio}
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-3 col-md-6 col-sm-12 col-xs-6">
              <div className="techno_flipbox mb-30">
                <div className="techno_flipbox_font">
                  <div className="techno_flipbox_inner">
                    <div className="techno_flipbox_icon">
                      <div className="icon">
                        <i className="flaticon-call" />
                      </div>
                    </div>
                    <div className="flipbox_title">
                      <h3>{data?.company?.services?.onlineSupport}</h3>
                    </div>
                  </div>
                </div>
                <div className="techno_flipbox_back">
                  <div className="techno_flipbox_inner">
                    <div className="flipbox_title">
                      <h3>{data?.company?.services?.onlineSupport}</h3>
                    </div>
                    <div className="flipbox_desc pt-3">
                      <p>
                        {data?.company?.services?.onLigneSupportDescription}
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-3 col-md-6 col-sm-12 col-xs-6">
              <div className="techno_flipbox mb-30">
                <div className="techno_flipbox_font">
                  <div className="techno_flipbox_inner">
                    <div className="techno_flipbox_icon">
                      <div className="icon">
                        <i className="flaticon-developer" />
                      </div>
                    </div>
                    <div className="flipbox_title">
                      <h3> {data?.company?.services?.qualityProduct}</h3>
                    </div>
                  </div>
                </div>
                <div className="techno_flipbox_back">
                  <div className="techno_flipbox_inner">
                    <div className="flipbox_title">
                      <h3> {data?.company?.services?.qualityProduct}</h3>
                    </div>
                    <div className="flipbox_desc pt-3">
                      <p>
                        {data?.company?.services?.qualityProductDescription}
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-3 col-md-6 col-sm-12 col-xs-6">
              <div className="techno_flipbox mb-30">
                <div className="techno_flipbox_font">
                  <div className="techno_flipbox_inner">
                    <div className="techno_flipbox_icon">
                      <div className="icon">
                        <i className="flaticon-global-1" />
                      </div>
                    </div>
                    <div className="flipbox_title">
                      <h3>{data?.company?.services?.productivSoftware}</h3>
                    </div>
                  </div>
                </div>
                <div className="techno_flipbox_back">
                  <div className="techno_flipbox_inner">
                    <div className="flipbox_title">
                      <h3>{data?.company?.services?.productivSoftware}</h3>
                    </div>
                    <div className="flipbox_desc pt-3">
                      <p>
                        {data?.company?.services?.productivSoftwareDescription}
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}
