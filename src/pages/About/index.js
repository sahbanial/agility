import data from "config/data";
import Images from "config/Images";
import Layout from "layout/Layout";
import React from "react";

export default function About() {
  return (
    <Layout pageTitle={data?.menu?.company}>
      <>
        <div className="about_area pt-85 pb-70">
          <div className="container">
            <div className="row">
              <div className="col-lg-6 col-md-6 col-sm-12 col-xs-6">
                <div className="about_thumb">
                  <img src={Images.tab1} alt />
                </div>
              </div>
              <div className="col-lg-6 col-md-6 col-sm-12 col-xs-6">
                <div className="section_title text_left mb-40 mt-3">
                  <div className="section_sub_title uppercase mb-3">
                    <h6>{data?.company?.yearExperice}</h6>
                  </div>
                  <div className="section_main_title">
                    <h1>{data?.home?.aboutUsTitle}</h1>
                    <h1>
                      {data?.home?.aboutUsTitle2}
                      <span>IT Solutions.</span>
                    </h1>
                  </div>
                  <div className="em_bar">
                    <div className="em_bar_bg" />
                  </div>
                  <div className="section_content_text bold pt-5">
                    <p>{data?.home?.aboutUsDescription}</p>
                  </div>
                </div>
                <div className="singel_about_left mb-30">
                  <div className="singel_about_left_inner mb-3">
                    <div className="singel-about-content boder pl-4">
                      <p>{data?.company?.companySubDescription}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/*==================================================*/}
        {/*--- End Techno About Area ---*/}
        {/*==================================================*/}
        {/*==================================================*/}
        {/*--- Start Techno Flipbox Top Feature Area ---*/}
        {/*==================================================*/}
        <div className="flipbox_area top_feature pb-70 two">
          <div className="container">
            <div className="row">
              <div className="col-lg-3 col-md-6 col-sm-12 col-xs-6">
                <div className="techno_flipbox mb-30">
                  <div className="techno_flipbox_font">
                    <div className="techno_flipbox_inner">
                      <div className="techno_flipbox_icon">
                        <div className="icon">
                          <i className="flaticon-code" />
                        </div>
                      </div>
                      <div className="flipbox_title">
                        <h3>{data?.company?.services?.responsiveDesign}</h3>
                      </div>
                    </div>
                  </div>
                  <div className="techno_flipbox_back">
                    <div className="techno_flipbox_inner">
                      <div className="flipbox_title">
                        <h3>{data?.company?.services?.responsiveDesign}</h3>
                      </div>
                      <div className="flipbox_desc pt-3">
                        <p>
                          {data?.company?.services?.responsiveDesginDescriptio}
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg-3 col-md-6 col-sm-12 col-xs-6">
                <div className="techno_flipbox mb-30">
                  <div className="techno_flipbox_font">
                    <div className="techno_flipbox_inner">
                      <div className="techno_flipbox_icon">
                        <div className="icon">
                          <i className="flaticon-call" />
                        </div>
                      </div>
                      <div className="flipbox_title">
                        <h3>{data?.company?.services?.onlineSupport}</h3>
                      </div>
                    </div>
                  </div>
                  <div className="techno_flipbox_back">
                    <div className="techno_flipbox_inner">
                      <div className="flipbox_title">
                        <h3>{data?.company?.services?.onlineSupport}</h3>
                      </div>
                      <div className="flipbox_desc pt-3">
                        <p>
                          {data?.company?.services?.onLigneSupportDescription}
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg-3 col-md-6 col-sm-12 col-xs-6">
                <div className="techno_flipbox mb-30">
                  <div className="techno_flipbox_font">
                    <div className="techno_flipbox_inner">
                      <div className="techno_flipbox_icon">
                        <div className="icon">
                          <i className="flaticon-developer" />
                        </div>
                      </div>
                      <div className="flipbox_title">
                        <h3> {data?.company?.services?.qualityProduct}</h3>
                      </div>
                    </div>
                  </div>
                  <div className="techno_flipbox_back">
                    <div className="techno_flipbox_inner">
                      <div className="flipbox_title">
                        <h3> {data?.company?.services?.qualityProduct}</h3>
                      </div>
                      <div className="flipbox_desc pt-3">
                        <p>
                          {data?.company?.services?.qualityProductDescription}
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg-3 col-md-6 col-sm-12 col-xs-6">
                <div className="techno_flipbox mb-30">
                  <div className="techno_flipbox_font">
                    <div className="techno_flipbox_inner">
                      <div className="techno_flipbox_icon">
                        <div className="icon">
                          <i className="flaticon-global-1" />
                        </div>
                      </div>
                      <div className="flipbox_title">
                        <h3>{data?.company?.services?.productivSoftware}</h3>
                      </div>
                    </div>
                  </div>
                  <div className="techno_flipbox_back">
                    <div className="techno_flipbox_inner">
                      <div className="flipbox_title">
                        <h3>{data?.company?.services?.productivSoftware}</h3>
                      </div>
                      <div className="flipbox_desc pt-3">
                        <p>
                          {
                            data?.company?.services
                              ?.productivSoftwareDescription
                          }
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    </Layout>
  );
}
