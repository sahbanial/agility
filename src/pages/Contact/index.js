import data from "config/data";
import Layout from "layout/Layout";
import React from "react";

export default function Contact() {
  return (
    <Layout pageTitle="Contacter nous">
      <>
        <div className="contact_address_area pt-80 pb-70">
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <div className="section_title text_center mb-55">
                  <div className="section_sub_title uppercase mb-3">
                    <h6>CONTACTER NOUS</h6>
                  </div>
                  <div className="section_main_title">
                    <h1>Nous sommes là pour vous</h1>
                  </div>
                  <div className="em_bar">
                    <div className="em_bar_bg" />
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-4 col-md-6 col-sm-12">
                <div className="single_contact_address text_center mb-30">
                  <div className="contact_address_icon pb-3">
                    <i className="fa fa-map-o" />
                  </div>
                  <div className="contact_address_title pb-2">
                    <h4>Enter Your Address</h4>
                  </div>
                  <div className="contact_address_text">
                    <p> {data?.address} </p>
                  </div>
                </div>
              </div>
              <div className="col-lg-4 col-md-6 col-sm-12">
                <div className="single_contact_address text_center mb-30">
                  <div className="contact_address_icon pb-3">
                    <i className="fa fa-clock-o" />
                  </div>
                  <div className="contact_address_title pb-2">
                    <h4>Horaires d'ouvertures</h4>
                  </div>
                  <div className="contact_address_text">
                    <p>Lun - Ven: 8:00 - 17:00</p>
                  </div>
                </div>
              </div>
              <div className="col-lg-4 col-md-6 col-sm-12">
                <div className="single_contact_address text_center mb-30">
                  <div className="contact_address_icon pb-3">
                    <i className="fa fa-volume-control-phone" />
                  </div>
                  <div className="contact_address_title pb-2">
                    <h4>Contactez directement</h4>
                  </div>
                  <div className="contact_address_text">
                    <p>{`${data?.email} ,${data?.tel}`}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/*==================================================*/}
        {/*--- Start Techno Contact Area ---*/}
        {/*==================================================*/}
        <div className="main_contact_area pt-80 bg_color2 pb-90">
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <div className="section_title text_center mb-55">
                  <div className="section_sub_title uppercase mb-3">
                    <h6>CONTACTER NOUS</h6>
                  </div>
                  <div className="section_main_title">
                    <h1>N'hésitez pas à contacter</h1>
                    <h1>Nous maintenant</h1>
                  </div>
                  <div className="em_bar">
                    <div className="em_bar_bg" />
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-xl-12">
                <div className="contact_from">
                  <form
                    id="contact_form"
                    action="http://html.dreamitsolution.net/techno/mail.php"
                    method="POST"
                  >
                    <div className="row">
                      <div className="col-lg-6">
                        <div className="form_box mb-30">
                          <input type="text" name="name" placeholder="Name" />
                        </div>
                      </div>
                      <div className="col-lg-6">
                        <div className="form_box mb-30">
                          <input
                            type="email"
                            name="email"
                            placeholder="Email"
                          />
                        </div>
                      </div>
                      <div className="col-lg-6">
                        <div className="form_box mb-30">
                          <input type="text" name="phone" placeholder="Tél" />
                        </div>
                      </div>
                      <div className="col-lg-6">
                        <div className="form_box mb-30">
                          <input type="text" name="web" placeholder="Website" />
                        </div>
                      </div>
                      <div className="col-lg-12">
                        <div className="form_box mb-30">
                          <textarea
                            name="message"
                            id="message"
                            cols={30}
                            rows={10}
                            placeholder="Ecrire un message"
                            defaultValue={""}
                          />
                        </div>
                        <div className="quote_btn text_center">
                          <button className="btn" type="submit">
                            Envoyer
                          </button>
                        </div>
                      </div>
                    </div>
                  </form>
                  <p className="form-message" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    </Layout>
  );
}
