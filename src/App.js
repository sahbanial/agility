import { AppContextProvider } from "context/AppContext";
import React from "react";
import Router from "./Router";

export default function App() {
  return (
    <AppContextProvider>
      <Router />
    </AppContextProvider>
  );
}
